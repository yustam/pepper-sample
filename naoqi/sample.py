# -*- encoding: UTF-8 -*-
import time

import qi
from naoqi import ALProxy

pepper_ip = "192.168.8.16"
pepper_port = 9559


# pepper_ip = "localhost"
# pepper_port = 61159


def init_motion():
    motion = ALProxy("ALMotion", pepper_ip, pepper_port)
    motion.setStiffnesses("Body", 1.0)
    motion.moveInit()
    return motion


def pepper_walk(x, y, r):
    motion = init_motion()
    motion.moveTo(x, y, r)


def pepper_hand():
    motion = init_motion()
    motion.openHand('RHand')


def pepper_say(text):
    # speech = ALProxy("ALTextToSpeech", pepper_ip, pepper_port)
    speech = ALProxy("ALAnimatedSpeech", pepper_ip, pepper_port)
    # speech.setParameter("pitchShift", 0)
    # speech.setParameter("doubleVoice", 2.0)
    # speech.setVoice("Kenny22Enhanced")
    # speech.setLanguage("English")
    # speech.setLanguage("Japanese")
    # print(speech.getAvailableVoices())
    # print(speech.getAvailableLanguages())
    speech.say(text)


def pepper_move(motion, angle):
    # motion = init_motion()
    id = motion.post.changeAngles(["LShoulderPitch", "LShoulderRoll"], angle, 0.3)
    # motion.wait(id, 0)
    time.sleep(1)


def pepper_tablet():
    local = "tcp://127.0.0.1:53940"
    remote = "tcp://192.168.8.16:53943"
    sd = qi.Session()
    # sd.listenStandalone("tcp://" + pepper_ip + ":" + str(pepper_port))
    # sd.listenStandalone(remote)
    sd.listenStandalone(remote)

    session = qi.Session()
    session.connect(sd.endpoints()[0])
    session.listenStandalone(local)
    time.sleep(0.01)

    print(session.isConnected())
    print(session.endpoints())
    # session.listenStandalone()
    tablet = session.service("ALTabletService")
    # tablet = ALProxy("ALTabletService", pepper_ip, pepper_port)
    tablet.enableWifi()
    tablet.configureWifi('wpa', 'Newton', 'rec0ch09')
    print(tablet.getWifiStatus())
    tablet.showWebview()
    tablet.loadUrl('http://recochoku.jp')
    session.close()


if __name__ == '__main__':
    pepper_walk(0, 0, 2)
    # pepper_say("hello, pepper!")
    # pepper_say("こんにちは")
    # pepper_hand()
    # motion = init_motion()
    # motion.setCollisionProtectionEnabled("Arms", False)
    # for i in range(5):
    #     pepper_move(motion, [5.0, -2.0])
    #     pepper_move(motion, [0.0, 1.0])
    # pepper_tablet()

