# -*- coding: utf-8 -*-
import datetime
import json
import sqlite3
import time
import urllib2

from naoqi import ALProxy

pepper_ip = "192.168.8.16"
pepper_port = 9559


# pepper_ip = "localhost"
# pepper_port = 61159


def pepper_action(text):
    speech = ALProxy("ALTextToSpeech", pepper_ip, pepper_port)
    speech.setParameter("pitchShift", 1.5)
    speech.post.say(text)
    motion = ALProxy("ALMotion", pepper_ip, pepper_port)
    motion.setCollisionProtectionEnabled("LArm", False)
    motion.setTangentialSecurityDistance(0.0)
    motion.setOrthogonalSecurityDistance(0.0)
    motion.setStiffnesses("Body", 1.0)
    motion.moveInit()
    for x in range(2):
        ids = []
        id = motion.post.changeAngles(["LShoulderPitch", "LShoulderRoll"], [5.0, -2.5], 0.2)
        time.sleep(1)
        id = motion.post.setAngles(["LShoulderPitch", "LShoulderRoll"], [0.0, 2.0], 0.3)
        time.sleep(1)
        for id in ids:
            motion.wait(id, 0)
    time.sleep(1)
    id = motion.post.setAngles(["LShoulderPitch", "LShoulderRoll"], [0.0, 0.0], 0.3)
    time.sleep(1)
    for id in ids:
        motion.wait(id, 0)
    motion.moveInit()


def get_status(ip_address):
    data = json.load(urllib2.urlopen('http://' + ip_address + '/status.json'))
    print(data)
    return {
        'ip': ip_address,
        'date': str(data['update_date']).split('.')[0],
        'open': 1 if (data['status'] == '0') else 0
    }


def save_status(status):
    conn = sqlite3.connect('./pepper.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS status
    (ip TEXT, date TEXT, open INT)''')
    c.execute("INSERT INTO status VALUES ('%s', '%s', %d)"
              % (status['ip'], status['date'], status['open']))
    conn.commit()
    c.close()


def summary_status(ip_address):
    conn = sqlite3.connect('./pepper.db')
    c = conn.cursor()
    five_minutes_ago = datetime.datetime.now() + datetime.timedelta(minutes=-1)
    date_str = five_minutes_ago.strftime('%Y-%m-%d %H:%M:%S')
    print(date_str)
    c.execute("SELECT sum(open) FROM status WHERE ip = '%s' AND date > '%s'"
              % (ip_address, date_str))
    for row in c:
        print(row)
        if row[0] == 0:
            pepper_action("もう出ろよ")


if __name__ == '__main__':
    ip_address = '192.168.8.24'
    while True:
        status = get_status(ip_address)
        print(status)
        save_status(status)
        summary_status(ip_address)
        time.sleep(5)
